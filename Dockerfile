FROM php:8.2-fpm-alpine3.19 AS php

# Install other software
RUN apk add --update \
    supervisor \
    nginx \
    unzip \
    libxml2-dev \
    oniguruma-dev \
    curl-dev \
    zip \
    nodejs \
    npm

## Install PHP extentions
RUN docker-php-ext-install curl dom mbstring pdo_mysql opcache

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
