.PHONY: help temp

IMAGE_NAME=base-php-nginx-composer-npm-image
CONTAINER_NAME=base-php-nginx-composer-npm

help: ## Print help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

build: ## Build the image
	@docker build . -t ${IMAGE_NAME}

build-clean: ## Build the image w/o cache
	@docker build . --no-cache -t ${IMAGE_NAME}
